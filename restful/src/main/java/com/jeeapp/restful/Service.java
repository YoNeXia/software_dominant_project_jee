package com.jeeapp.restful;

import com.jeeapp.model.Application;
import com.jeeapp.model.File;
import com.jeeapp.model.Message;
import com.jeeapp.model.Operation;
import com.jeeapp.model.User;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * REST Web Service
 *
 * @author YoNeXia
 */
@RequestScoped
@Path("")
public class Service implements IService {
    
    @Inject
    private JMSContext jmsContext;
    
    @Resource(lookup = "jms/checkQueue")
    private Queue checkQueue;
    
    /**
     * Creates a new instance of Service
     */
    public Service() {
    }
    
    /**
     * Retrieves representation of an instance of com.jeeapp.restful.Service
     * @return an instance of java.lang.String
     */
    @Override
    public String testService() {
        return "{\"message\":\"Hello from REST!\"}";
    }
    
    /**
     * POST method for updating or creating an instance of Service
     * @param content representation for the resource sended by client
     * @return Response for client
     */
    @Override
    public Response checkFile(String content) {
        Message message = new Message();
        
        try (JsonReader jreader = Json.createReader(new StringReader(content))) {
            
            setMessage(message, jreader.readObject());
            Boolean isValid = sendFileToQueue(message);
            Response response = null;
                        
            if (isValid) {
                response = Response.accepted().entity("Informations sended to JEE server.").build();
            } else {
                response = Response.status(400).entity("An error has occured... ").build();
            }
            
            return response;
        } catch ( JsonParsingException | NullPointerException e) {
            return Response.status(400).entity(e).build();
        }
    }
    
    /**
     * Method to send message in JMS queue
     * @param message message send in the JMS queue
     * @return boolean to know if message is sended in the queue
     */
    private boolean sendFileToQueue(Message message) {
        JAXBContext jaxbContext;
        
        try {
            jaxbContext = JAXBContext.newInstance(Message.class);
            
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            StringWriter writer = new StringWriter();
            
            jaxbMarshaller.marshal(message, writer);
            TextMessage msg = jmsContext.createTextMessage(writer.toString());
            
            jmsContext.createProducer().send(checkQueue, msg);
            
            return true;
        } catch (JAXBException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    /**
     * Method to set any message
     * @param message message send in the JMS queue
     * @param file
     * @param user
     * @param application
     * @param operation
     * @param data
     * @return Message
     */
    private Message setMessage(Message message, JsonObject jsonObject) {
        File file = new File();
        User user = new User();
        Application application = new Application();
        Operation operation = new Operation();
        
        file.setTitle(jsonObject.getJsonArray("Files").getJsonObject(0).getString("Title"));
        file.setKey(jsonObject.getJsonArray("Files").getJsonObject(0).getString("Key"));
        file.setContent(jsonObject.getJsonArray("Files").getJsonObject(0).getString("ContentString"));
        
        user.setToken(jsonObject.getJsonObject("User").getString("Token"));
        
        application.setVersion(jsonObject.getJsonObject("Application").getString("Version"));
        
        operation.setName(jsonObject.getJsonObject("Operation").getString("Name"));
        operation.setInfos(jsonObject.getJsonObject("Operation").getString("Infos"));
        operation.setVersion(jsonObject.getJsonObject("Operation").getString("Version"));
        
        message.setUser(user);
        message.setFile(file);
        message.setApplication(application);
        message.setOperation(operation);
        
        System.out.println(file.getTitle());
        
        return message;
    }
}
