package com.jeeapp.restful;

import javax.ws.rs.core.Application;

/**
 * Class to instantiate the REST API
 *
 * @author YoNeXia
 */
@javax.ws.rs.ApplicationPath("services")
public class ApplicationConfig extends Application {}
