package com.jeeapp.restful;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Interface to expose methods availible in API
 *
 * @author YoNeXia
 */
public interface IService {
    /**
     * Retrieves representation of an instance of com.jeeapp.restful.Service
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    String testService();
    
    /**
     * POST method for updating or creating an instance of Service
     * @param content representation for the resource sended by client
     * @return Response for client
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    Response checkFile(String content);
}
