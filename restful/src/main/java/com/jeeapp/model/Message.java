package com.jeeapp.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Structure of any message used in API
 *
 * @author YoNeXia
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Message implements Serializable {
    /**
     * Instance of class Operation
     */
    @XmlElement
    private Operation operation;
    /**
     * Instance of class Application
     */
    @XmlElement
    private Application application;
    /**
     * Instance of class User
     */
    @XmlElement
    private User user;
    /**
     * Instance of list of files
     */
    @XmlElement
    private File file;
    
    /**
     * Get the instance of operation
     * @return the instance of operation
     */
    public Operation getOperation() {
        return this.operation;
    }
    /**
     * Set operation
     * @param operation operation to set
     */
    public void setOperation(Operation operation) {
        this.operation = operation;
    }
    
    /**
     * Get the instance of application
     * @return the instance of application
     */
    public Application getApplication() {
        return this.application;
    }
    /**
     * Set application
     * @param application application to set
     */
    public void setApplication(Application application) {
        this.application = application;
    }
    
    /**
     * Get the instance of user
     * @return the instance of user
     */
    public User getUser() {
        return this.user;
    }
    /**
     * Set user
     * @param user user to set
     */
    public void setUser(User user) {
        this.user = user;
    }
    
    /**
     * Get the instance of file
     * @return the instance of file
     */
    public File getFile() {
        return this.file;
    }
    /**
     * Set file
     * @param file file to set
     */
    public void setFile(File file) {
        this.file = file;
    }
}
