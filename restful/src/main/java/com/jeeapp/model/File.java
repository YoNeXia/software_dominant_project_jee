package com.jeeapp.model;

/**
 * Structure of any file used in API
 *
 * @author YoNeXia
 */
public class File {
    /**
     * File name
     */
    private String title;
    /**
     * File key finded to decrypt the file
     */
    private String key;
    /**
     * File data is the content of file
     */
    private String content;
    /**
     * File data is the secretInformation of file
     */
    private String secretInformation;
    
    
    /**
     * Get the title
     * @return the title
     */
    public String getTitle() {
        return this.title;
    }
    /**
     * Set title
     * @param title title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
     * Get the key
     * @return the key
     */
    public String getKey() {
        return this.key;
    }
    /**
     * Set key
     * @param key key to set
     */
    public void setKey(String key) {
        this.key = key;
    }
    
    /**
     * Get the content
     * @return the content
     */
    public String getContent() {
        return this.content;
    }
    /**
     * Set content
     * @param content content to set
     */
    public void setContent(String content) {
        this.content = content;
    }
    
    /**
     * Get the secretInformation
     * @return the secretInformation
     */
    public String getSecretInformation() {
        return this.secretInformation;
    }
    /**
     * Set secretInformation
     * @param secretInformation secretInformation to set
     */
    public void setSecretInformation(String secretInformation) {
        this.secretInformation = secretInformation;
    }
}
