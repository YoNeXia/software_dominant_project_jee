package com.jeeapp.model;

/**
 * Structure of application parameters
 *
 * @author YoNeXia
 */
public class Application {
    /**
     * Application version
     */
    private String version;
    
    /**
     * Application token
     */
    private String token;
    
    
    /**
     * Get the version
     * @return the version
     */
    public String getVersion() {
        return this.version;
    }
    /**
     * Set version
     * @param version version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }
    
    /**
     * Get the token
     * @return the token
     */
    public String getToken() {
        return this.token;
    }
    /**
     * Set token
     * @param token token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
}
