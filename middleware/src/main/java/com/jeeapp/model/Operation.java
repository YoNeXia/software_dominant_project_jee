package com.jeeapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Structure of any file used in API
 *
 * @author YoNeXia
 */
@XmlRootElement(name = "operation")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Operation {
    /**
     * Operation status
     */
    private boolean status;
    /**
     * Operation name
     */
    private String name;
    /**
     * Operation infos
     */
    private String infos;
    /**
     * Operation version
     */
    private String version;
    
    
    /**
     * Get the status
     * @return the status
     */
    public boolean getStatus() {
        return this.status;
    }
    /**
     * Set status
     * @param status status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }
    
    /**
     * Get the name
     * @return the name
     */
    public String getName() {
        return this.name;
    }
    /**
     * Set name
     * @param name version to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Get the infos
     * @return the infos
     */
    public String getInfos() {
        return this.infos;
    }
    /**
     * Set infos
     * @param infos version to set
     */
    public void setInfos(String infos) {
        this.infos = infos;
    }
    
    /**
     * Get the version
     * @return the version
     */
    public String getVersion() {
        return this.version;
    }
    /**
     * Set version
     * @param version version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }
    
}
