package com.jeeapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Structure of any user used in API
 *
 * @author YoNeXia
 */
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class User {
    /**
     * User login
     */
    private String login;
    /**
     * User password
     */
    private String password;
    /**
     * User token
     */
    private String token;
    
    
    /**
     * Get the login
     * @return the login
     */
    public String getLogin() {
        return this.login;
    }
    /**
     * Set login
     * @param login login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }
    /**
     * Get the password
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }
    /**
     * Set password
     * @param password password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    /**
     * Get the token
     * @return the token
     */
    public String getToken() {
        return this.token;
    }
    /**
     * Set token
     * @param token token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
}
