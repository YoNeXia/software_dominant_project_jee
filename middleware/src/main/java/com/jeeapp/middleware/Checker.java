package com.jeeapp.middleware;

import com.jeeapp.dao.WordDAO;
import com.jeeapp.wsdl.ArrayOfFile;
import com.jeeapp.wsdl.ObjectFactory;
import com.jeeapp.wsdl.Service;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Class to check file and find secret information inside
 *
 * @author YoNeXia
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/checkQueue")
        ,
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class Checker implements MessageListener {
    List<String> dictionnary = new WordDAO().findAllWords();
    String secretInformation;
    
    /**
     * Method to get all message from JMS queue
     * @param msg message returned from JMS queue
     */
    @Override
    public void onMessage(Message msg) {
        JAXBContext jaxbContext;
        
        try {
            String content = msg.getBody(String.class);
            
            // XML string form Message to Message object
            jaxbContext = JAXBContext.newInstance(com.jeeapp.model.Message.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            
            com.jeeapp.model.Message message = (com.jeeapp.model.Message) jaxbUnmarshaller.unmarshal(new StringReader(content));
            
            List<Boolean> findedWords = findWords(selectSample(message.getFile().getContent()), dictionnary);
            
            if(calculateConfidenceLevel(findedWords) < 0.55) {
                System.out.println("Bad confidence level...");
            } else {
                secretInformation = searchSecretInformation(message.getFile().getContent());
                System.out.println("SECRET FOUNDED! " + secretInformation);
                
                sendAnswer(message);
            }
        } catch (JMSException | JAXBException e) {
            Logger.getLogger(Checker.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /**
     * Method to select a sample of text from file
     * @param fileContent text from file sended by clients
     * @return an array of strings, sample of file
     */
    public List<String> selectSample(String fileContent) {
        List<String> fileWords = new ArrayList<>();
        int sampleSize = 100;
        
        if(Arrays.asList(fileContent.split("[ -.?!,;:(){}']+")).size() <= sampleSize) {
            return Arrays.asList(fileContent.split("[ -.?!,;:(){}']+"));
        } else {
            int random = new Random().nextInt(Arrays.asList(fileContent.split("[ -.?!,;:(){}']+")).size() - sampleSize);
            List<String> tmp = Arrays.asList(fileContent.split("[ -.?!,;:(){}']+"));
            
            for(int i = random; i < random + sampleSize; i++) {
                fileWords.add(tmp.get(i));
            }
            
            return fileWords;
        }
    }
    
    /**
     * Method to detetct if a sample of words from file are in french
     * @param sample list of word extract from file
     * @param dictionnary list of word extract from database
     * @return an array of boolean, each value represent a word in french or not
     */
    public List<Boolean> findWords(List<String> sample, List<String> dictionnary) {
        boolean finded = false;
        List<Boolean> findedWords = new ArrayList<>(Arrays.asList(new Boolean[0]));
        
        for(String value : sample) {
            for(String word: dictionnary) {
                if (value != null && value.toLowerCase().equals(word.toLowerCase())) {
                    finded = true;
                    break;
                }
            }
            findedWords.add(finded);
            finded = false;
        }
        
        return findedWords;
    }
    
    /**
     * Method to calculate a confidence level
     * @param words list of boolean to know if words was finded or not
     * @return a float, the confidence level
     */
    public float calculateConfidenceLevel(List<Boolean> words) {
        return (float)words.stream().filter(p -> p == true).count() / (float)words.size();
    }
    
    /**
     * Method to search a secret information in file
     * @param fileContent content of file needed to find secret information
     * @return a string, the secret information if founded
     */
    public String searchSecretInformation(String fileContent) {
        String[] tmp_1 = fileContent.split("information s");
        String[] tmp_2 = fileContent.split("Information s");
        
        if (tmp_1[1] != null || tmp_2 != null) {
            String[] tmp_3 = tmp_1[1].split("[.!?]");
            String[] tmp_4 = tmp_3[0].split("[.!?:]");
            return tmp_4[1];
        } else {
            return "File decrypted but no secret information founded...";
        }
    }
    
    /**
     * Method to send a message to an API
     * @param message message build before send it to .NET API
     */
    public void sendAnswer(com.jeeapp.model.Message message) {
        ObjectFactory factory = new ObjectFactory();
        
        com.jeeapp.wsdl.Operation operation = new com.jeeapp.wsdl.Operation();
        operation.setStatut(true);
        operation.setName(factory.createOperationName("SendSecretFile"));
        operation.setInfos(factory.createOperationInfos("Response of file verification"));
        operation.setVersion(factory.createOperationVersion("1.0"));
        
        com.jeeapp.wsdl.User user = new com.jeeapp.wsdl.User();
        user.setLogin(factory.createUserLogin(message.getUser().getLogin()));
        user.setPassword(factory.createUserPassword(message.getUser().getPassword()));
        user.setToken(factory.createUserToken(message.getUser().getToken()));
        
        com.jeeapp.wsdl.Application application = new com.jeeapp.wsdl.Application();
        application.setToken(factory.createApplicationToken("17CD7EA1939BF964F36A81BB3D28C"));
        application.setVersion(factory.createApplicationVersion(message.getApplication().getVersion()));
        
        com.jeeapp.wsdl.File file = new com.jeeapp.wsdl.File();
        file.setTitle(factory.createFileTitle(message.getFile().getTitle()));
        file.setKey(factory.createFileKey(message.getFile().getKey()));
        file.setContentString(factory.createFileContentString(message.getFile().getContent()));
        com.jeeapp.wsdl.ArrayOfFile arrayOfFile = new ArrayOfFile();
        arrayOfFile.getFile().add(file);
        
        
        com.jeeapp.wsdl.Message msg = new com.jeeapp.wsdl.Message();
        msg.setOperation(factory.createOperation(operation));
        msg.setApplication(factory.createApplication(application));
        msg.setUser(factory.createUser(user));
        msg.setFiles(factory.createArrayOfFile(arrayOfFile));
        
        Service service = new Service();
        service.getBasicHttpBindingIService().dispatch(msg);
    }
}
