package com.jeeapp.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class WordDAO to access Oracle database data
 *
 * @author YoNeXia
 */
public class WordDAO {
    
    /**
     * Instance of connection with Oracle database
     */
    public Connection connection;
    /**
     * List of words availible in database
     */
    public List<String> words;
    
    /**
     * Method to find all words in database
     * @return an array of strings, words from database
     */
    public List<String> findAllWords(){
        words = new ArrayList<>();
        ResultSet resultSet = null;

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/orcl.localdomain", "sys as sysdba", "1234");

            resultSet = connection.prepareStatement("select * from french").executeQuery();

            while (resultSet.next()) {
                words.add(resultSet.getString("word"));
            }

            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
        
        return this.words;
    }
}
